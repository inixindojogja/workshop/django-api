from django.urls import path
from .views import *

urlpatterns = [
    path('api/sendemail', sendEmail, name="api_sendemail"),
    path('api/get-resep', getResep, name="api_getresep"),
    path('menu-resep', showMenuResep, name="view_resep"),


    path('karyawan', showKaryawan, name="t_karyawan"),
    path('api/get-karyawan', getKaryawan, name="api_get-karyawan"),
    path('api/upd-karyawan', updKaryawan, name="api_upd-karyawan"),
]
